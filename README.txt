
This module is an extension of the original archive module.  It makes the archive calender blogcentric so that only the current blog's user's blog entries are displayed in the calendar.  It further adds the $uid to the path, and adds an additional form select on the archives page so that you can browse the blog archives by user.

You should be able to safely use this module along with the original archive module.

TODO:  Currently if you change the user on the blogarchive page, the PATH is not updated and as such the calendar block does not get updated with the new users info.  Ultimately the block needs to check the form information as well as the path for the user ID or the form needs to update the url when it posts...

  Suggestions are welcome.   I am a member of a discussion list for discussion
of this module and related personal blogging features.

    http://softwaremanagers.org/drupal-personal-blogs


Project page: http://drupal.org/project/blogtheme


Module Author:
David Hill a.k.a. Tatonca  <tatonca_@hotmail.com>

